We provide repair services for all brands and types of heating, cooling equipment and household appliances. Pillar offers many maintenance services also to care of your equipment, not only to protect your investment, but adding to the length of life and efficiency of your equipment. Call us today!

Address: 1524 E Haycraft Ave, Coeur d'Alene, ID 83815, USA

Phone: 208-964-6350

Website: https://pillarheatingandair.repair
